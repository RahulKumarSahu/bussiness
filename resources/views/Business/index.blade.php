<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootstrap Table</title>
    
    <!-- Include Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">
        
    {{-- <a href="url('/create')" class="btn btn-primary">Create Business</a> --}}
    <a href="{{ route('create') }}" class="btn btn-primary">Create</a>

    <a href="{{ route('branch.create') }}" class="btn btn-primary">Create Branch</a>

    <h1>Business Table</h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($businessData as $value)
                                    <tr>
                                        <td>{{ $value->id }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>{{ $value->phone_number }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <form action="{{ route('delete', $value->id) }}" method="post">
                                                    <input type="hidden" name="id" value="{{ $value->id }}">
                                                    @csrf
                                                    <input value="delete" type="submit"
                                                        onclick="return confirm('Are you sure to detele')"
                                                        class="btn btn-danger">
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

        </tbody>
    </table>
</div>

<!-- Include Bootstrap JS (optional) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
