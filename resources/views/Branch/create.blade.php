<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Parsley.js Example</title>
   <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/gh/guillaumepotier/Parsley.js@2.9.2/dist/parsley.js"></script>
</head>
<body>
   <h2>Fill in the form and click the validate button at the bottom</h2>

   <form method="POST" action="{{ route('branch.store') }}" enctype="multipart/form-data">
    @csrf
      <label for="heard">Select Business Name *:</label>
      <select id="heard" name ="business_id">
         <option value="">Choose..</option>
         @foreach ($businessData as $value )
         <option value="{{ $value->id }}">{{ $value->name }}</option>
         @endforeach
      </select>
      <br>
      <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Name</label>
        <input type="text" class="form-control"  name="name" id="name">
      </div>
      <br>

      <input type="checkbox"  value="monday" parsley-error-message="Please selct day" name="day[]"/><label>Monday</label>
      <label for="start_time">Start Time:</label>
      <input type="time" name="start_time[]" parsley-error-message="Please selct start time" id="start_time" >
      <label for="start_time">End Time:</label>
      <input type="time"  name="end_time[]" parsley-error-message="Please selct end time" id="end_time" >
      <br>
      <input type="checkbox"  value="tuesday" parsley-error-message="Please selct day"  name="day[]"/><label>Tuesday</label>
      <label for="start_time">Start Time:</label>
      <input type="time"  name="start_time[]" parsley-error-message="Please selct start time" id="start_time" >
      <label for="start_time">End Time:</label>
      <input type="time"  name="end_time[]" parsley-error-message="Please selct end date" id="end_time" >
      <br>
      <input type="checkbox"  value="wednesday" parsley-error-message="Please selct day" name="day[]"/><label>Wednesday</label>
      <label for="start_time">Start Time:</label>
      <input type="time"  name="start_time[]"parsley-error-message="Please selct start date" id="start_time" >
      <label for="start_time">End Time:</label>
      <input type="time"  name="end_time[]" parsley-error-message="Please selct end date" id="end_time" >
      <br>
      <input type="checkbox"  value="thursday" parsley-error-message="Please selct day" name="day[]"/><label>Thursday</label>
      <label for="start_time">Start Time:</label>
      <input type="time"  name="start_time[]" parsley-error-message="Please selct start time" id="start_time" >
      <label for="start_time">End Time:</label>
      <input type="time"  name="end_time[]" parsley-error-message="Please selct end time" id="end_time" >
      <br>
      <input type="checkbox"  value="Friday" parsley-error-message="Please selct day" name="day[]"/><label>Friday</label>
      <label for="start_time">Start Time:</label>
      <input type="time"  name="start_time[]" parsley-error-message="Please selct start date" id="start_time" >
      <label for="start_time">End Time:</label>
      <input type="time"  name="end_time[]" parsley-error-message="Please selct end date" id="end_time" >
      <br>
      <input type="checkbox"  value="Saturday" name="day[]"/><label>Saturday</label>
      <label for="start_time">Start Time:</label>
      <input type="time"  name="start_time[]" parsley-error-message="Please selct start date" id="start_time" >
      <label for="start_time">End Time:</label>
      <input type="time"  name="end_time[]" parsley-error-message="Please selct end date" id="end_time" >
      <br>
      <input type="checkbox" value="Sunday" name="day[]"/><label>Sunday</label>
      <label for="start_time">Start Time:</label>
      <input type="time" parsley-error-message="Please selct start date" name="start_time[]" id="start_time">
      <label for="start_time">End Time:</label>
      <input type="time" name="end_time[]" parsley-error-message="Please selct end date" id="end_time" >
      <br>
      <label for="start_time">Image:</label>
      <input type="file" name="images[]" multiple>

      {{-- <input type="submit" class="btn btn-default" value="Validate"> --}}
      <button type="submit" class="btn btn-primary">Submit</button>
   </form>
   <script>
      $(function() {
         $('#demo-form').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
         })
         .on('form:submit', function() {
            return false;
         });
      });
   </script>
</body>
</html>
