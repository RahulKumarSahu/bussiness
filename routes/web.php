<?php

use App\Http\Controllers\BranchController;
use App\Http\Controllers\BusinessController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[BusinessController::class,'index'])->name('index');
Route::get('/create',[BusinessController::class,'create'])->name('create');
Route::post('/store',[BusinessController::class,'store'])->name('store');
Route::post('/delete',[BusinessController::class,'delete'])->name('delete');

Route::get('/branch/create',[BranchController::class,'create'])->name('branch.create');
Route::post('branch/store',[BranchController::class,'store'])->name('branch.store');

