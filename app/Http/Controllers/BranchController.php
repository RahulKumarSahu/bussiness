<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Business;
use App\Models\WorkingDay;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $businessData = Business::get();
        return view('Branch.create', compact('businessData'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $branch = new Branch();
        $branch->b_id = $request->business_id;
        $branch->name = $request->name;

        $request->validate([
            'images.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        $imgs="";
    
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {

                $imageName = time() . '-' . $image->getClientOriginalName();
                $image->move(public_path('uploads'), $imageName);
                $imgs.=$imageName.",";
                // Image::create(['name' => $imageName]);
            }
        }
        $branch->image=$imgs;
        $branch->save();
        $day = $request->day;

        $starttime = $request->start_time;
        $endtime = $request->end_time;

        if (isset($day)) {
            foreach ($day as $key => $value) {
                // dd($value);
                $dayTable = new WorkingDay();
                $dayTable->branch_id = $branch->id;
                $dayTable->day = $value;
                $dayTable->start_time = $starttime[$key];
                $dayTable->end_time = $endtime[$key];
                $dayTable->save();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
